require_relative '../lib/bober'

place_url = "https://maps.google.com/?cid=16904231133254439631"

Bober.start!
at_exit { Bober.stop! }

Bober.create_session(place_url,
  viewportWidth: 1280,
  userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:51.0) Gecko/20100101 Firefox/51.0'
)

Bober.session.wait_for_content("SHARE")
Bober.session.find_element('button', title: "SHARE").click

Bober.session.wait_for_content("Embed map")
Bober.session.find_element('button', title: "Embed map").click

Bober.session.wait_element_appear('[name="embedHtml"]')

puts "IFRAME URL:"
puts Bober.session.field_value('[name="embedHtml"]')


# Bober.session.show_image
