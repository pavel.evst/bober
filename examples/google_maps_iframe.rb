require_relative '../lib/bober'

place_share_url = "https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15772." + 
  "076894241005!2d115.138502!3d-8.784261!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%" + 
  "3A0xea97e4a73abad2cf!2sRock+Bar!5e0!3m2!1sen!2sid!4v1486267351458"

Bober.start!
at_exit { Bober.stop! }

Bober.create_session("https://vt-stage.info/frame.html")

Bober.session.execute(%{
  document.getElementById('frame').src = "#{place_share_url}"
})

Bober.session.wait_iframe_loading

Bober.session.execute_driver_js(%{
this.webpage.switchToFrame(this.webpage.framesName[0]);
})

Bober.session.wait_for_content("review")

content = Bober.session.page_text
puts "PLACE REVIEWS: " + content[/\d+ review/][/\d+/]

#Bober.session.show_image