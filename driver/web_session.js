var system = require('system');
var fs = require('fs');
var qs = require('./qs');
var webserver = require("webserver").create();
var log = require('./logger');

var WebPageServer = false; // don't include here because server.js requires this file


class WebSession {
  constructor(sessionId) {
    this.createdAt = new Date();
    this.sessionId = sessionId;
  }

  loadPage(url, options = {}, callback) {
    log.info('loading page', url);
    var defaultOptions = {
      redirectTimeout: 30, // ms
      viewportWidth: 1024,
      viewportHeight: 800,
      backlistUrls: []
    };
    this.options = Object.assign({}, defaultOptions, options || {});
    this.pageLoading = false;
    this.loadingPages = [];
    this.loadingIframes = [];
    this.pageLoadCallbacks = [];
    this.iframeLoadCallbacks = [];
    this.downloads = [];
    this.alerts = [];

    if (url === undefined || url === null || url === '') {
      throw new Error("Missing required parameter 'url'");
    }

    this.webpage = require('webpage').create();
    this.webpage.viewportSize = { width: this.options.viewportWidth, height: this.options.viewportHeight };
    if (this.options.userAgent) {
      this.webpage.settings.userAgent = this.options.userAgent;
    }

    this.webpage.onLoadStarted = function(url, isFrame) {
      if (isFrame) {
        log.debug("Start loading iframe", url);
        //this.loadingPages.push(url);
      } else {
        this.pageLoading = true;
      }
    }.bind(this);

    this.webpage.onLoadFinished = function(status, url, isFrame) {
      //console.log('Loading finished: ' + url + ' is a '+ status);
      //this.pageFinishLoading(url);
    }.bind(this);

    this.webpage.onResourceError = (id, url, errorCode, errorString) => {
      //console.log("Resource Error", url, errorCode, errorString);
    };

    this.webpage.onConsoleMessage = (message, line, file) => {
      log.info('LOG:', message, `${file}:${line}`);
    };

    this.webpage.onLongRunningScript = (message) => {
      log.info("Long script detected:", message);
    };

    this.webpage.onNavigationRequested = (url, type, willNavigate, main) => {
      if (willNavigate) {
        if (main) {
          this.pageStartLoading(url);
        } else {
          this.iframeStartLoading(url);
        }
      }
      if (main) {
        log.debug('Navigate to: ' + url, type, willNavigate);
      } else {
        log.debug('Frame navigate to: ' + url, type, willNavigate);
      }
    };

    this.webpage.onResourceRequested = (requestData, networkRequest) => {
      //console.log('Request (#' + requestData.id + '): ' + JSON.stringify(requestData));
      var skipper = this.options.backlistUrls.find((url) => {
        return requestData.url.indexOf(url) != -1;
      });
      if (skipper) {
        log.debug("Skiping", requestData.url);
        networkRequest.abort();
        return;
      }

      //var ajax = "X-Requested-With"
    };

    this.webpage.onResourceReceived = (response) => {
      if (response.stage == 'end') {
        if (this.loadingPages.length) {
          var currentPage = this.loadingPages[this.loadingPages.length - 1];
          if (response.url == currentPage) {
            var locationHeader = response.headers.find((header) => {
              return header.name == 'Location';
            });
            if (locationHeader) {
              log.debug("Redirect", currentPage, "->", locationHeader.value);
              this.pageFinishLoading(currentPage);
              this.pageStartLoading(this.resolveRelative(locationHeader.value, currentPage));
            } else {
              this.pageFinishLoading(currentPage);
            }
          }
        }
        if (this.loadingIframes.length) {
          if (this.loadingIframes.indexOf(response.url) != -1) {
            var locationHeader = response.headers.find((header) => {
              return header.name == 'Location';
            });
            if (locationHeader) {
              log.debug("Redirect", currentPage, "->", locationHeader.value);
              this.iframeFinishLoading(response.url);
              this.iframeStartLoading(this.resolveRelative(locationHeader.value, response.url));
            } else {
              this.iframeFinishLoading(response.url);
            }
          }
        }
        //if (response.url == 'https://devpay.garuda-indonesia.com/payment/cardinfo') {
        //  console.log('Response (#' + response.id + ', stage "' + response.stage + '"): ' + JSON.stringify(response));
        //}
        //console.log('got', response.url, JSON.stringify(response));
      }
    };

    this.webpage.onError = (message, stack) => {
      log.error("Page Error", message, JSON.stringify(stack));
    };

    this.webpage.onAlert = (text) => {
      log.info("ALERT", text);
      this.alerts.push(text);
    }

    this.webpage.onFileDownload = (url, responseData) => {
      if (!WebPageServer) {
        WebPageServer = require('./server');
      }
      var saveFilename = fs.join(WebPageServer.tmpFolder, `${this.sessionId}-${responseData.filename}`);
      this.downloads.push({filename: responseData.filename, path: saveFilename, content_type: responseData.contentType});
      log.info("FILEDOWNLOAD:", url, JSON.stringify(responseData));
      return saveFilename;
    };

    this.webpage.onFileDownloadError = (message) => {
      log.error('FILEDOWNLOADERROR:', message);
    };

    this.webpage.open(url).then(() => {
      //log.debug('Loaded', url);
      callback(this)
    });
  }

  setOption(key, value) {
    this.options.key = value;
  }

  pageStartLoading(url) {
    log.debug('START LAOD:', url);
    this.loadingPages.push(url);
  }

  iframeStartLoading(url) {
    log.debug('START IFRAME:', url);
    if (this.loadingIframes.includes(url)) {
      log.debug('SKIP IFRAME QUEUE (duplicate):', url);
    } else {
      this.loadingIframes.push(url);
    }
  }

  iframeFinishLoading(url) {
    log.debug('FINISH LAOD IFRAME:', url, this.loadingIframes.length);
    var index = this.loadingIframes.indexOf(url);
    if (index != -1) {
      this.loadingIframes.splice(index, 1);
    }
    if (this.finisgIframeTimeout) {
      clearTimeout(this.finisgIframeTimeout);
      this.finisgIframeTimeout = null;
    }
    if (this.loadingIframes.length == 0) {
      //console.log('this.options.redirectTimeout', this.options.redirectTimeout);
      this.finisgIframeTimeout = setTimeout(() => { // wait for if page has some redirect via JS
        if (this.loadingIframes.length == 0) {
          while (true) {
            var fn = this.iframeLoadCallbacks.shift();
            if (fn) { fn(status, url); } else { break; }
          }
        } else {
          log.debug("this.loadingIframes not empty", JSON.stringify(this.loadingIframes));
        }
        this.finisgIframeTimeout = null;
      }, this.options.redirectTimeout);
    }
  }

  resetAlerts() {
    this.alerts = [];
  }

  resolveRelative(path, base) {
    // Absolute URL
    if (path.match(/^[a-z]*:\/\//)) {
      return path;
    }
    // Protocol relative URL
    if (path.indexOf("//") === 0) {
      return base.replace(/\/\/.*/, path)
    }
    // Upper directory
    if (path.indexOf("../") === 0) {
        return resolveRelative(path.slice(3), base.replace(/\/[^\/]*$/, ''));
    }
    // Relative to the root
    if (path.indexOf('/') === 0) {
        var match = base.match(/(\w*:\/\/)?[^\/]*\//) || [base];
        return match[0] + path.slice(1);
    }
    //relative to the current directory
    return base.replace(/\/[^\/]*$/, "") + '/' + path.replace(/^\.\//, '');
  }

  pageFinishLoading(url) {
    log.debug('FINISH LAOD:', url, this.loadingPages.length);
    var index = this.loadingPages.indexOf(url);
    if (index != -1) {
      this.loadingPages.splice(0, index + 1);
    }
    if (this.finisgLoadTimeout) {
      clearTimeout(this.finisgLoadTimeout);
      this.finisgLoadTimeout = null;
    }
    if (this.loadingPages.length == 0) {
      //console.log('this.options.redirectTimeout', this.options.redirectTimeout);
      this.finisgLoadTimeout = setTimeout(() => { // wait for if page has some redirect via JS
        if (this.loadingPages.length == 0) {
          while (true) {
            var fn = this.pageLoadCallbacks.shift();
            if (fn) { fn(status, url); } else { break; }
          }
        } else {
          log.debug("this.loadingPages not empty", JSON.stringify(this.loadingPages));
        }
        this.finisgLoadTimeout = null;
      }, this.options.redirectTimeout);
    }
  }

  isLoading() {
    return this.loadingPages.length > 0;
  }

  isIframeLoading() {
    return this.loadingIframes.length > 0;
  }

  findElements(selector, title, parentId) {
    this._ensurePage();

    return this.webpage.evaluate(function (selector, title, parentId) {
      if (!window.__bober__) window.__bober__ = {};
      var scope = document;
      if (parentId && parentId != '') {
        scope = window.__bober__[parentId];
      }
      var elements = scope.querySelectorAll(selector);
      /*
      if (elements.length == 0) {
        throw `Can not find elements '${selector}'`;
      }
      */
      if (title && title != '') {
        var elements = Array.prototype.filter.call(elements, (element) => {
          return element.innerText == title;
        });
        /*
        if (elements.length == 0) {
          throw `Can not find elements '${selector}' with content '${title}'`;
        }
        */
      }
      return Array.prototype.map.call(elements, (element) => {
        var boberId = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
        window.__bober__[boberId] = element;
        var data = {tag: element.tagName.toLowerCase(), content: element.innerText, attributes: {}, bober_id: boberId};
        Array.prototype.forEach.call(element.attributes, (attr, i) => {
          data.attributes[attr.name] = attr.nodeValue;
        });
        return data;
      });
    }, selector, title, parentId);
  }

  getFieldValue(selector) {
    this._ensurePage();

    var result = this.webpage.evaluate(function (selector) {
      try {
        var asSelector = null;
        try {
          asSelector = document.querySelector(selector);
        } catch (e) {}

        var element = asSelector ||
                      document.querySelector(`[name='${selector}']`) ||
                      document.getElementById(selector);

        if (element) {
          return {status: 'ok', value: element.value};
        } else {
          return {status: 'error', message: "Can not find element"};
        }
      } catch (e) {
        return {status: 'error', message: error.message};
      }
    }, selector);

    if (result.status == 'ok') {
      return result.value;
    } else {
      throw new Error("setFieldValue: " + result.message);
    }
  }

  setFieldValue(selector, value) {
    this._ensurePage();

    var result = this.webpage.evaluate(function (selector, value) {
      try {
        var asSelector = null;
        try {
          asSelector = document.querySelector(selector);
        } catch (e) {}

        var element = asSelector ||
                      document.querySelector(`[name='${selector}']`) ||
                      document.getElementById(selector);

        if (!element) {
          throw new Error("can not find field " + selector);
        }

        if (element.tagName == 'INPUT' && element.type == 'checkbox') {
          element.checked = value == 'true' || value == '1';
        } else {
          element.value = value;
        }

        // if select and values wasn't set, try find value by options' label
        if (element.tagName == "SELECT" && element.value != value) {
          Array.prototype.forEach.call(element.querySelectorAll('option'), (option) => {
            if (option.innerText.trim() == value) {
              console.log('set select', option.value, option.innerText);
              element.value = option.value;
            }
          });
        }

        element.dispatchEvent(new Event('keyup'));
        element.dispatchEvent(new Event('change'));
        return {status: 'ok'};
      } catch (error) {
        return {status: 'error', message: '' + error.message};
      }
    }, selector, value);

    return result;
  }

  getScreenshot() {
    if (this.webpage === undefined) throw new Error("page is not initialized");

    return this.webpage.renderBase64('png');
  }

  pageSource() {
    this._ensurePage();
    return this.webpage.frameContent;
  }

  pageText() {
    this._ensurePage();
    return this.webpage.evaluate(function () {
      return document.body && document.body.innerText;
    });
  }

  pageUrl() {
    this._ensurePage();
    return this.webpage.url;
  }

  loadUrl(url) {
    this._ensurePage();
    return this.webpage.open(url);
  }

  pageTitle() {
    this._ensurePage();
    return this.webpage.title;
  }

  pageHasContent(searchable) {
    this._ensurePage();

    var searchableRegex = new RegExp(searchable, 'm');

    var pageText = this.pageText();

    return pageText ? searchableRegex.test(pageText) : false;
  }

  pageHasCss(selector, visible) {
    this._ensurePage();

    return this.webpage.evaluate(function (selector, visible) {
      var elements = document.querySelectorAll(selector);
      if (visible && visible != '') {
        var visibleElement = Array.prototype.find.call(elements, (e) => {
          return e.clientWidth !== 0 &&
                 e.clientHeight !== 0 &&
                 e.style.opacity !== 0 &&
                 e.style.visibility !== 'hidden' &&
                 e.style.display !== 'none';
        });
        return !!visibleElement;
      } else {
        return elements.length > 0;
      }
    }, selector, visible);
  }

  submitForm(selector) {
    this._ensurePage();

    var result = this.webpage.evaluate(function (selector) {
      var form;
      if (selector) {
        form = document.querySelector(selector);
      } else {
        form = document.querySelectorAll('form')[0];
      }
      if (form) {
        HTMLFormElement.prototype.submit.call(form);
      } else {
        throw new Error("Can not find form" + (selector ? ` with selector '${selector}'` : ''));
      }
    }, selector);

    return this._wait_loading(result);
  }

  clickLink(selector, title, visible = true) {
    var result;
    if (title && title != '') {
      result = this.webpage.evaluate(function (title, selector, visible) {
        var tags = [];
        var found = [];
        if (selector && selector !== '') {
          tags = document.querySelectorAll(selector);
          if (tags.length == 0) {
            throw new Error("Can not find link with selector '" + selector + "'");
          }
        } else {
          tags = document.getElementsByTagName("a");
        }

        for (var i = 0; i < tags.length; i++) {
          if (tags[i].tagName == 'INPUT' && tags[i].value == title || tags[i].textContent == title) {
            found.push(tags[i]);
          }
        }

        if (visible && visible != '') {
          found = found.filter((el) => {
            if (!el.offsetHeight && !el.offsetWidth) { return false; }
            if (getComputedStyle(el).visibility === 'hidden') { return false; }
            return true;
          });
        }

        if (found.length > 1) {
          throw new Error("More then one link found with title '" + title + "'");
        } else if (found.length == 0) {
          throw new Error("Can not find link or button with title '" + title + "'");
        }

        found[0].click();
      }, title, selector, visible);
    } else if (selector && selector != '') {
      result = this.webpage.evaluate(function (selector) {
        var element = document.querySelector(selector);
        if (element === null) {
          throw new Error("Can not find link with selector '" + selector + "'");
        }
        element.click();
      }, selector);
    } else {
      throw new Error("Missing argument: title or selector is required");
    }

    return this._wait_loading(result);
  }

  clickElement(selector, boberId) {
    if (selector && selector != '') {
      return this.webpage.evaluate(function (selector) {
        try {
          var element = document.querySelector(selector);
          if (element) {
            element.click();
          } else {
            throw new Error("Can not find element with selector '" + selector + "'");
          }
          return {status: 'ok'};
        } catch (error) {
          return {status: 'error', message: '' + error.message};
        }
      }, selector);
    } else if (boberId && boberId != '') {
      return this.webpage.evaluate(function (boberId) {
        try {
          if (!window.__bober__) {
            throw new Error("can't find element (window.__bober__ not defined)");
          }
          if (!(boberId in window.__bober__)) {
            throw new Error("can't find element");
          }
          window.__bober__[boberId].click();
          return {status: 'ok'};
        } catch (error) {
          return {status: 'error', message: '' + error.message};
        }
      }, boberId);
    } else {
      return {status: 'error', message: 'clickElement: selector or boborId is required'};
    }
  }

  getTableData(selector) {
    return this.webpage.evaluate(function (selector) {
      try {
        var table = document.querySelector(selector);
        if (!table) {
          throw new Error("Can not find table with selector '" + selector + "'");
        }
        var boberId = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
        if (!window.__bober__) window.__bober__ = {};
        window.__bober__[boberId] = table;
        var result = {bober_id: boberId, attributes: {}, content: []};
        Array.prototype.forEach.call(table.attributes, (attr, i) => {
          result.attributes[attr.name] = attr.nodeValue;
        });

        Array.prototype.forEach.call(table.querySelectorAll('tr'), (row, i) => {
          var cells = [];
          Array.prototype.forEach.call(row.querySelectorAll('td, th'), (cell) => {
            cells.push(cell.innerText)
          });
          result.content.push(cells);
        });

        return {status: 'ok', table: result};
      } catch (error) {
        console.log(error.stack);
        return {status: 'error', message: '' + error.message};
      }
    }, selector);
  }

  executeJs(code, args) {
    //var fn = new Function(`return (${code})`);
    var fn = new Function(code);
    var value = this.webpage.evaluate.apply(this.webpage, [fn].concat(args));
    //console.log('execute', code, value);
    return value;
  }

  executeDriverJs(code) {
    var fn = new Function(code);
    return fn.call(this);
  }

  waitForUrl(url, timeout) {
    if (timeout === undefined || timeout === null || timeout === '') timeout = 10;
    var urlRegex = new RegExp(url);
    var promise = this._waitForFn(() => {
      log.trace('waitForUrl', this.webpage.url, urlRegex, urlRegex.test(this.webpage.url), this.isDomLoaded() ? 'DOM_LOADED' : 'DOM_PENDING');
      return urlRegex.test(this.webpage.url) && this.isDomLoaded();
    }, timeout);

    return promise.then((result) => {
      return {success: !!result, url: this.webpage.url};
    });
  }

  waitElementAppear(selector, visible, timeout) {
    return this._waitForFn(() => {
      return this.pageHasCss(selector, visible);
    }, timeout);
  }

  waitForContent(content, timeout) {
    if (timeout === undefined || timeout === null || timeout === '') timeout = 10;
    return this._waitForFn(() => {
      return this.pageHasContent(content);
    }, timeout);
  }

  waitDownloads(filename, timeout) {
    if (timeout === undefined || timeout === null || timeout === '') timeout = 10;
    if (filename === undefined || filename === null || filename === '') filename = false;

    return this._waitForFn(() => {
      if (filename) {
        var nameRegex = new RegExp(filename);
        var file = this.downloads.find((file) => {
          return nameRegex.test(file.filename);
        });
        return file && fs.exists(file.path);
      } else {
        return this.downloads.length > 0 && fs.exists(this.downloads[0].path);
      }
    }, timeout);
  }

  isDomLoaded() {
    var docState = this.webpage.evaluate(function () {
      return document.readyState;
    });
    //console.log('document state:', docState);
    return docState == 'interactive' || docState == 'complete';
  }

  _waitForFn(callback, timeout, interval = 50) {
    return new Promise((resolve, reject) => {
      if (callback()) {
        resolve(true);
      } else {
        var startTime = Date.now();
        var i = setInterval(() => {
          if (callback()) {
            clearInterval(i);
            resolve(this._wait_loading(true));
            //resolve(true);
          } else if (Date.now() > startTime + timeout * 1000) {
            clearInterval(i);
            resolve(false);
          }
        }, interval);
      }
    });
  }

  close(removeItself) {
    try {
      if (this.webpage) {
        this.webpage.stopJavaScript();
        this.webpage.stop();
        this.webpage.clearCookies();
        this.webpage.close();
      }
      if (removeItself) {
        delete WebSession.Sessions[this.sessionId];
      }
      return true;
    } catch (error) {
      log.error(error);
      log.error(error.message);
      log.error(error.stack);
      return false;
    }
  }

  waitIframeLoading(result) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        if (this.isIframeLoading()) {
          this.iframeLoadCallbacks.push(function (status, url) {
            resolve(result === undefined ? true : result);
          });
        } else {
          resolve(result === undefined ? true : result);
        }
      }, this.options.redirectTimeout);
    });
  }

  _wait_loading(result) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        if (this.isLoading()) {
          this.pageLoadCallbacks.push(function (status, url) {
            resolve(result);
          });
        } else {
          resolve(result);
        }
      }, this.options.redirectTimeout);
    });
  }

  _ensurePage() {
    if (this.webpage === undefined) throw new Error("page is not initialized");
  }
}

WebSession.Sessions = {};

module.exports = WebSession;