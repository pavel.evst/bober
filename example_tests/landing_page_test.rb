require_relative 'test_helper'

describe "Testing landing page" do
  include Bober::TestDSL

  it "should load test booking page" do
    load_page("https://devpay.garuda-indonesia.com/payment/test_booking",
      redirectTimeout: 30,
      backlistUrls: [
        'assets.adobedtm.com',
        'cdn.mxpnl.com',
        'stats.g.doubleclick.net',
        'www.googletagmanager.com',
        's.yjtag.jp',
        'fonts.gstatic.com'
      ]
    )
    page.submit_form
    page.wait_for_url(%r{/payment/cardinfo/.+})
    assert(page.has_content?("Pay With Credit Card"))
    #show_image
  end

end