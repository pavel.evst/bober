require_relative 'test_helper'

describe "Testing admin tools" do
  include Bober::TestDSL

  def login!(after_login = nil)
    load_page("http://localhost:3002/admin/sessions/new" + (after_login ? "?after_login=#{after_login}" : ''))

    page.fill_in('#email', 'lemur@example.com')
    page.fill_in('#password', 'Password01')
    page.submit_form

    page.wait_for_content('You are logged in')
    assert_equal(after_login || '/admin', page.path)
  end

  it "should login" do
    login!('/admin/help')

    assert_equal('/admin/help', page.path)
    assert(page.has_content?('You are logged in'))
  end

  it "should download list of payments" do
    login!('/admin/payments')

    page.click_link title: 'Excel'
    downloads = page.downloads
    assert_equal(downloads.size, 1)
    assert_equal(downloads[0]['filename'], 'garuda_payments.xlsx')
    assert_equal(downloads[0]['content_type'], 'application/octet-stream')
    assert_includes(downloads[0]['path'], '/tmp/slimerjs-')
    assert_includes(downloads[0]['path'], '-garuda_payments.xlsx')
  end

  it "should download timezones" do
    login!('/admin/ga_timezones')

    page.click_link title: 'Download CSV'
    downloads = page.downloads

    csv = File.read(downloads[0]['path'])
    assert_equal(csv.lines.first, "CityCode,CityName,AirportCode,GMTTZ,TimeZone,TZName," +
                                  "airport_code,airport_name,city_name,country_code,tz_name,tz_offset\n")
  end
end