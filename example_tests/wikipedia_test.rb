require_relative 'test_helper'

describe "Testing wikipedia" do
  include Bober::TestDSL

  it "should open wikipedia page" do
    load_page("http://wikipedia.org")

    assert_equal('https://www.wikipedia.org/', page.url)
    assert_equal('Wikipedia', page.title)
    #page.wait_for_content('The Free Encyclopedia')
    assert(page.has_content?('The Free Encyclopedia'))
    assert(page.has_content?(/English[\s\S]+Deutsch/))
  end
end
