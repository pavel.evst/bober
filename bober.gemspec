Gem::Specification.new do |gem|
  gem.specification_version = 3

  gem.name = 'bober'
  gem.version = "0.1"
  gem.authors = ["Pavel Evstigneev"]
  gem.email = ["pavel.evst@gmail.com"]
  gem.license = 'MIT'
  gem.date = '2017-01-13'
  gem.summary = "Browser testing library build for speed"
  gem.homepage = 'https://gitlab.dev.veritrans.co.id/pavel/bober'

  gem.files = `git ls-files`.split("\n")
  gem.require_path = 'lib'

  gem.required_ruby_version = '>= 2.0'

  gem.add_dependency 'websocket-driver', ['>= 0.6', '<2']
  gem.add_dependency 'concurrent-ruby', ['>= 1.0', '<2']
end
