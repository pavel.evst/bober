module Bober
  class Runner

    attr_accessor :commander

    def start!(port:, output: STDOUT)
      return if defined?(@process_id) && @process_id

      @output = output || StringIO.new

      #child_env = {'NODE_ENV' => ENV['NODE_ENV'] || 'test', 'PORT' => '3133'}
      process_dir = File.expand_path('../../../driver', __FILE__)
      child_env = {"PORT" => port.to_s}
      slimerjs_args = ENV['SLIMERJS_OPTS'] || ''
      @process_id = spawn(child_env, "slimerjs #{slimerjs_args} ws_server.js", out: output || "/dev/null", chdir: process_dir)
      Bober.logger.info "Bober-driver is running! (pid: #{@process_id})"

      self
    end

    def stop!(output: nil)
      if output
        warn "'output' argument is depricated"
      end

      if output
        output.puts "Stopping server"
      else
        Bober.logger.info "Stopping server"
      end

      if @process_id
        Process.kill(:TERM, @process_id)
        Process.wait(@process_id)
      end
    end

    def port_open?(ip, port, seconds = 0.5)
      puts "Checking port #{ip}:#{port}" if ENV['DEBUG']
      Timeout::timeout(seconds) do
        begin
          TCPSocket.new(ip, port).close
          true
        rescue Errno::ECONNREFUSED, Errno::EHOSTUNREACH
          false
        end
      end
    rescue Timeout::Error
      false
    end

  end
end