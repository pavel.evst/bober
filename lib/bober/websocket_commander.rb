require 'json'
require 'timeout'
require 'concurrent/atomic/atomic_fixnum'

module Bober
  class WebsocketCommander

    def initialize(host = '127.0.0.1', port = 9005)
      @host = host
      @post = port
    end

    def start_server! #(host = '127.0.0.1', port = 9005)
      Bober.logger.info "Starting websocket server on #{@host}:#{@post}"
      @last_rid = Concurrent::AtomicFixnum.new
      @command_waiters = {}
      @connection_waiter = Utils::PubSub.new

      @server_runner = Thread.new do
        Thread.current.abort_on_exception = true
        @server = WebsocketServer.new(@host, @post)
        @has_connection = false

        while connection = @server.wait_connection
          #Bober.logger.info "client connected"
          @connection = connection
          @connection_waiter.notify(true)
          resend_message = if @message_handler
            Bober.logger.warn "Additional client connected, restarting thread"
            @message_handler.kill
            true
          end

          @message_handler = Thread.new do
            Thread.current.abort_on_exception = true
            while true
              message = connection.wait_message
              if message.is_a?(WebSocket::Driver::MessageEvent)
                on_message(message.data)
              else
                p [:NON_TEXT_MESSAGE, message]
              end
            end
          end
          if resend_message
            Bober.logger.info "Resending last command to new connection"
            resend_current_command
          end
        end
      end
    end

    def wait_connection
      @connection_waiter.wait
    end

    def create_rid
      @last_rid.increment
    end

    def send_command(command, arguments = {})
      rid = create_rid
      @command_waiters[rid] = Utils::Waiter.new
      @current_command = [rid, command, arguments]
      send_ws_message(rid, JSON.generate(@current_command))
    ensure
      @current_command = nil
    end

    def resend_current_command
      rid = @current_command.first
      send_ws_message(rid, JSON.generate(@current_command))
    ensure
      @current_command = nil
    end

    def send_ws_message(rid, message)
      tries = 0
      begin
        @connection.send_message(JSON.generate(@current_command))
        Timeout::timeout(90) do
          wait_command(rid)
        end
      rescue Timeout::Error => error
        Bober.logger.warn "ERROR WAITING RESPONSE! (90 sec) Retrying..."
        tries += 1
        retry if tries <= 3
        nil
      end
    end

    def close!
      if @connection
        send_command(:quit)
        @connection.close!
      end
    end

    def wait_command(rid)
      @command_waiters[rid].wait
    end

    def on_message(content)
      message = JSON.parse(content)
      @command_waiters[message.first].notify(message)
    end
  end
end
