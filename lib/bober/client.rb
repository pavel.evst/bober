require 'rack'

module Bober
  class Client
    def initialize(port)
      @port = port
    end

    def ping
      GET('/ping')
    end

    def quit
      GET('/quit')
    end

    def create_session(load_url: nil, options: {})
      POST('/session', query: Rack::Utils.build_nested_query(load_url: load_url, options: options))
    end

    def connection
      @connection ||= Excon.new("http://127.0.0.1:#{@port}", persistent: true)
    end

    def GET(path, options = {})
      puts "GET #{path} #{options[:query] ? options[:query] : ''}" unless path == '/ping'
      print_time do
        connection.get(options.merge(path: path))
      end
    end

    def POST(path, options = {})
      puts "POST #{path}"
      print_time do
        connection.post(options.merge(path: path))
      end
    end

    def print_time
      s_time = Time.now.to_f
      result = yield
      puts " -> Time #{(Time.now.to_f - s_time).round(3)} sec"
      result
    end

  end
end

