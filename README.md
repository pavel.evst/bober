Bober
=====

Browser testing library build for speed

### How to use

```ruby
require 'bober'

describe "Testing wikipedia" do
  include Bober::TestDSL

  it "should open wikipedia page" do
    load_page("http://wikipedia.org")

    assert_equal('https://www.wikipedia.org/', page.url)
    assert_equal('Wikipedia', page.title)
    assert(page.has_content?('The Free Encyclopedia'))
  end
end
```

### API

* `load_page` - load slimerjs, and load page
* `page` - page object (see session.rb for more feature)
* `show_image` - save screenshot and open it in Preview


### For scraping

```ruby
require 'bober'

place_url = "https://maps.google.com/?cid=16904231133254439631"

Bober.start!
at_exit { Bober.stop! }

Bober.create_session(place_url,
  viewportWidth: 1280,
  userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.12; rv:51.0) Gecko/20100101 Firefox/51.0'
)

Bober.session.wait_for_content("SHARE")
Bober.session.show_image
```

----


Install latest slimerjs

    sudo npm install -g laurentj/slimerjs


TODO:

* wait for alerts
* custom function to check if page starting to load and finish loading (e.g. check ajax or spa apps)
* screenshot uploading to clouds
